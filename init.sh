#!/bin/sh
HR="echo +------------------------------------+"

echo ".+*^*+.   Let's make things a bit more homey. .+*^*+."

 # Ask for the administrator password upfront
    sudo -v

# Keep-alive: update existing `sudo` time stamp until the script has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

$HR
echo "  Installing Homebrew..."
$HR
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

$HR
echo "  Installing iTerm2..."
$HR
brew cask install --appdir="~/Applications" iterm2

$HR
echo "  Installing VS Code..."
$HR
brew cask install --appdir="~/Applications" visual-studio-code

$HR
echo "  Installing Slack..."
$HR
brew cask install --appdir="/Applications" slack

echo "Would you like to install Android tools?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) ./android.sh; break;;
        No ) exit;;
    esac
done